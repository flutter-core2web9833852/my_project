import 'package:flutter/material.dart';
import 'package:myproject/HomePage.dart';
import 'package:myproject/signin.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => SignUpState();
}

class SignUpState extends State<SignUp> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _newPasswordController = TextEditingController();
  final TextEditingController _mailController = TextEditingController();

  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  // List<UserCredential> userCredentialList = [];

  // userSignUp([UserCredential? credentialObj]) {
  //   setState(() {
  //     //  index++;
  //     credentialObj!.username = _nameController.text.trim();
  //     credentialObj.email = _mailController.text.trim();
  //     credentialObj.password = _newPasswordController.text.trim();
  //   });
  //   clearController();
  // }

  // void clearController() {
  //   _nameController.clear();
  //   _newPasswordController.clear();
  //   _mailController.clear();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Container(
          // height: MediaQuery.of(context).size.height * 1,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  stops: [
                0.1,
                0.3
              ],
                  colors: [
                Color.fromRGBO(0, 139, 148, 1),
                Color.fromRGBO(255, 255, 255, 1)
              ])),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 80,
                  ),
                  Padding(
                    padding: EdgeInsets.all(25.0),
                    child: Text(
                      "Welcome",
                      style: TextStyle(
                          fontSize: 35,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: Form(
                  key: _formkey,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.67,
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(50),
                            topRight: Radius.circular(50)),
                        boxShadow: [
                          BoxShadow(offset: Offset(0.1, -0.1), blurRadius: 5)
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.all(22),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 30,
                          ),
                          TextFormField(
                            controller: _nameController,
                            decoration: InputDecoration(
                              hintText: "Enter Name",
                              label: const Text("Name"),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(25)),
                              prefixIcon: const Icon(Icons.person_rounded),
                            ),
                            validator: (value) {
                              print("In name Validator");
                              if (value == null || value.isEmpty) {
                                return "Please enter name";
                              } else {
                                return null;
                              }
                            },
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          TextFormField(
                            controller: _mailController,
                            decoration: InputDecoration(
                              hintText: "Enter Email Address",
                              label: const Text("Email Address"),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(25)),
                              prefixIcon: const Icon(Icons.person_rounded),
                            ),
                            keyboardType: TextInputType.emailAddress,
                            validator: (value) {
                              print("In email validator");
                              if (value == null || value.isEmpty) {
                                return "Please enter email address";
                              } else {
                                return null;
                              }
                            },
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          TextFormField(
                            controller: _newPasswordController,
                            obscureText: true,
                            obscuringCharacter: "*",
                            decoration: InputDecoration(
                              hintText: "Enter Password",
                              label: const Text("Password"),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(25)),
                              prefixIcon: const Icon(Icons.password_rounded),
                            ),
                            validator: (value) {
                              print("In password validator");
                              if (value == null || value.isEmpty) {
                                return "Please enter password";
                              } else {
                                return null;
                              }
                            },
                          ),
                          const SizedBox(
                            height: 22,
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SignIn()));
                            },
                            child: const Text(
                              "Already have an account? Sign In",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(119, 118, 118, 1)),
                            ),
                          ),
                          const SizedBox(height: 80),
                          GestureDetector(
                            onTap: () {
                              bool signupValidated =
                                  _formkey.currentState!.validate();

                              if (signupValidated) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      backgroundColor: Colors.green,
                                      content: Text("Sign Up Successfully!")),
                                );
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => HomePage()));
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        backgroundColor: Colors.red,
                                        content: Text("Sign Up Failed!")));
                              }
                            },
                            child: Container(
                              width: 200,
                              height: 60,
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(0, 139, 148, 1),
                                  borderRadius: BorderRadius.circular(50)),
                              child: const Center(
                                child: Text(
                                  "Sign Up",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 22,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 0,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

