

class listModelClass {
  int? id;
  String name;
  String mail;
  String date;
  String position;
  String medals;
  listModelClass(
      {this.id,
      required this.name,
      required this.mail,
      required this.date,
      required this.medals,
      required this.position});
  Map<String, dynamic> MapFun() {
    return {
      'name': name,
      'mail': mail,
      'date': date,
      'medals': medals,
      'position': position
    };
  }
}