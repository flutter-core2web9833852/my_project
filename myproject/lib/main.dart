import 'package:flutter/material.dart';
import 'package:myproject/HomePage.dart';
import 'package:myproject/signup.dart';
 import 'package:sqflite/sqflite.dart';
 import 'package:path/path.dart' as path;
 import 'package:myproject/ListPage.dart';


 dynamic database;

void main() async{

    WidgetsFlutterBinding.ensureInitialized();

  database = openDatabase(
    path.join(await getDatabasesPath(), "myDB.db"),
    version: 1,
    onCreate: (db, version) {
      db.execute('''CREATE TABLE NavyClass(
ID INT PRIMARY KEY AUTOINCREMENT,
name TEXT,
position TEXT
date TEXT,
mail TEXT,
medals TEXT
)''');
    },
  );

  officerList = await getValues();

  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SignUp(),
      ),
    );
  }
}
