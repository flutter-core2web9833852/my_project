import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'package:myproject/modelClass.dart';
// import 'package:sqflite/sqflite.dart';
//import 'package:path/path.dart' as path;

class InfoList extends StatefulWidget {
  const InfoList({super.key});
  @override
  State<InfoList> createState() => _listAppState();
}

class _listAppState extends State<InfoList> {
  ///TEXT EDITING CONTROLLERS
  TextEditingController joinDateController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController mailController = TextEditingController();
  TextEditingController positionController = TextEditingController();
  TextEditingController medalsController = TextEditingController();

  void showBottomSheet(bool doedit, [listModelClass? listObj]) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
        ),
        isDismissible: true,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
                bottom: MediaQuery.of(context).viewInsets.bottom,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "ADD DETAILS",
                    style: GoogleFonts.quicksand(
                      fontWeight: FontWeight.w600,
                      fontSize: 22,
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        "Name",
                        style: GoogleFonts.quicksand(
                          color: const Color.fromRGBO(0, 139, 148, 1),
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      TextField(
                        controller: nameController,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromRGBO(0, 139, 148, 1),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: Colors.purpleAccent),
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      Text(
                        "Position",
                        style: GoogleFonts.quicksand(
                          color: const Color.fromRGBO(0, 139, 148, 1),
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      TextField(
                        controller: positionController,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromRGBO(0, 139, 148, 1),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: Colors.purpleAccent),
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      Text(
                        "Mail Id",
                        style: GoogleFonts.quicksand(
                          color: const Color.fromRGBO(0, 139, 148, 1),
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      TextField(
                        controller: mailController,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromRGBO(0, 139, 148, 1),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: Colors.purpleAccent),
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      Text(
                        "Medals",
                        style: GoogleFonts.quicksand(
                          color: const Color.fromRGBO(0, 139, 148, 1),
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      TextField(
                        controller: medalsController,
                        // maxLines: 4,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromRGBO(0, 139, 148, 1),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: Colors.purpleAccent),
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      Text(
                        "Joining Date",
                        style: GoogleFonts.quicksand(
                          color: const Color.fromRGBO(0, 139, 148, 1),
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      TextField(
                        controller: joinDateController,
                        readOnly: true,
                        decoration: InputDecoration(
                          suffixIcon: const Icon(Icons.date_range_rounded),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromRGBO(0, 139, 148, 1),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: Colors.purpleAccent),
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                        onTap: () async {
                          DateTime? pickeddate = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(2024),
                            lastDate: DateTime(2025),
                          );
                          String formatedDate =
                              DateFormat.yMMMd().format(pickeddate!);
                          setState(() {
                            joinDateController.text = formatedDate;
                          });
                        },
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: 50,
                    width: 300,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(30)),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
                      ),
                      onPressed: () {
                        doedit ? submit(doedit, listObj) : submit(doedit);

                        // if (nameController.text.isNotEmpty) {
                        Navigator.of(context).pop();
                        // }
                      },
                      child: Text(
                        "Submit",
                        style: GoogleFonts.inter(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                ],
              ),
            ),
          );
        });
  }

  // List<listModelClass> infoList = [];

  void submit(bool doedit, [listModelClass? listObj]) async {
    if (nameController.text.trim().isNotEmpty &&
        mailController.text.trim().isNotEmpty &&
        joinDateController.text.trim().isNotEmpty) {
      if (!doedit) {
        await insertOfficer(
          listModelClass(
            name: nameController.text.trim(),
            mail: mailController.text.trim(),
            date: joinDateController.text.trim(),
            position: positionController.text.trim(),
            medals: medalsController.text.trim(),
          ),
        );
        setState(() {});

        //  insertTaskData(todoList as ToDoModelClass);
        //   getTaskData();
      } else {
        setState(() {
          listObj!.date = joinDateController.text.trim();
          listObj.name = nameController.text.trim();
          listObj.mail = mailController.text.trim();
          listObj.medals = medalsController.text.trim();
          listObj.position = positionController.text.trim();
          updateInfo(listObj);
        });
      }
      //Navigator.of(context).pop();
    }

    clearController();
  }

  void clearController() {
    nameController.clear();
    mailController.clear();
    joinDateController.clear();
    positionController.clear();
    medalsController.clear();
  }

  void removeTasks(listModelClass listObj) {
    setState(() {
      officerList.remove(listObj);
      deleteOfficer(listObj);
    });
  }

  void editTask(listModelClass listObj) {
    nameController.text = listObj.name;
    mailController.text = listObj.mail;
    joinDateController.text = listObj.date;
    medalsController.text = listObj.medals;
    positionController.text = listObj.position;
    showBottomSheet(true, listObj);
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    joinDateController.dispose();
    mailController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 0, 242, 255),
        centerTitle: true,
        title: Text(
          "NAVY",
          style: GoogleFonts.quicksand(
            fontWeight: FontWeight.w700,
            fontSize: 25,
            color: Colors.white,
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: officerList.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 16,
            ),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.blueGrey,
                boxShadow: const [
                  BoxShadow(
                    offset: Offset(0, 10),
                    color: Color.fromRGBO(0, 0, 0, 0.1),
                    blurRadius: 10,
                  )
                ],
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    //1
                    Row(
                      children: [
                        Container(
                          height: 60,
                          width: 60,
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                          ),
                          child: const Icon(Icons.image_rounded),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                officerList[index].name,
                                style: GoogleFonts.quicksand(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 15,
                                  color: Color.fromARGB(255, 255, 255, 255),
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Text(
                                officerList[index].position,
                                style: GoogleFonts.quicksand(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromARGB(255, 255, 255, 255),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 14.0,
                    ),
                    //2
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Row(
                        children: [
                          Text(
                            officerList[index].date,
                            style: GoogleFonts.quicksand(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Color.fromARGB(255, 255, 255, 255),
                            ),
                          ),
                          const Spacer(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  editTask(officerList[index]);
                                },
                                child: const Icon(
                                  Icons.edit_outlined,
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              GestureDetector(
                                onTap: () {
                                  removeTasks(officerList[index]);
                                },
                                child: const Icon(
                                  Icons.delete_outline,
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 50,
            width: 150,
            child: FloatingActionButton(
                backgroundColor: Color.fromARGB(255, 0, 242, 255),
                onPressed: () {
                  clearController();
                  showBottomSheet(false);
                },
                child: const Text(
                  "Add Officer Info",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                )),
          ),
        ],
      ),
    );
  }
}

List officerList = [];

dynamic database;

void main() async {
//   WidgetsFlutterBinding.ensureInitialized();

//   database = openDatabase(
//     path.join(await getDatabasesPath(), "myDB.db"),
//     version: 1,
//     onCreate: (db, version) {
//       db.execute('''CREATE TABLE NavyClass(
// ID INT PRIMARY KEY AUTOINCREMENT,
// name TEXT,
// position TEXT
// date TEXT,
// mail TEXT,
// medals TEXT
// )''');
//     },
//   );

//   officerList = await getValues();

  runApp(const MainApp());
}


Future<void> insertOfficer(listModelClass obj) async {
  final localDB = await database;

  await localDB.insert('NavyClass', obj.MapFun(),
      conflictAlgorithm: ConflictAlgorithm.replace);

  officerList = await getValues();
}

Future<void> deleteOfficer(listModelClass obj) async {
  final localDB = await database;

  await localDB.delete('NavyClass', where: 'id = ?', whereArgs: [obj.id]);

  officerList = await getValues();
}

Future<void> updateInfo(listModelClass obj) async {
  final localDB = await database;

  await localDB
      .update('NavyClass', obj.MapFun(), where: 'id = ?', whereArgs: [obj.id]);

  officerList = await getValues();
}

Future<List<listModelClass>> getValues() async {
  final localDB = await database;

  List<Map<String, dynamic>> nlist = await localDB.query('NavyClass');

  return List.generate(nlist.length, (index) {
    return listModelClass(
        id: nlist[index]['id'],
        name: nlist[index]['name'],
        position: nlist[index]['position'],
        date: nlist[index]['date'],
        medals: nlist[index]['medals'],
        mail: nlist[index]['mail']);
  });

  //taskList = await getValues();
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(home: InfoList());
  }
}
